tomcat::instance { 'tomcat':
  catalina_base => '/opt/apache-tomcat/tomcat',
  source_url    => 'http://mirror.nexcess.net/apache/tomcat/tomcat-8/v8.0.8/bin/apache-tomcat-8.0.8.tar.gz'
}
tomcat::config::server { 'tomcat':
  catalina_base => '/opt/apache-tomcat/tomcat',
  port          => '8080',
}

