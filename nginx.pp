include nginx

nginx::resource::server { 'cnt7':
  listen_port => 8888,
  proxy       => 'http://localhost:8080',
}
